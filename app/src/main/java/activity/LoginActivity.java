package activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import models.Aplikasi;
import models.Daftar_siswa;
import models.Daftar_sma;

import com.example.absensito.R;

import JSON.JSON;
import adapter.SpinnerSMAAdapater;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by ZANDUT on 12/2/2016.
 */

public class LoginActivity extends AppCompatActivity implements InterfaceActivity{

    private EditText editNoTiket;
    private EditText editNama;
    private Spinner spinnerSekolah;
    private EditText editKelas;
    private EditText editAlamat;
    private Button btnMasuk;
    private Aplikasi aplikasi;
    private ArrayList<Daftar_sma> daftar_sma = new ArrayList<Daftar_sma>();
    private int status = 0;
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private class changeTyped implements OnKeyListener
    {
    	@Override
    	public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
    		// TODO Auto-generated method stub
    		if (editNoTiket.getText().toString().length()  == 4 && arg1 != KeyEvent.KEYCODE_DEL)
    		{
    			AsyncTask<String, String, JSONObject> task = new AsyncTask<String, String, JSONObject>(){
    				private ProgressDialog progress= new ProgressDialog(LoginActivity.this);
    				
    				protected void onPreExecute() {
    					
    					progress.setMessage(getString(R.string.loading));
    					progress.setCancelable(false);
    					progress.show();
    				};
    				
    				@Override
    				protected JSONObject doInBackground(String... arg0) {
    					// TODO Auto-generated method stub
    					JSON json = aplikasi.getJson();
    					json.setUrl(aplikasi.getDaftarUrl().get("getAllSiswa")+"/"+arg0[0]);
    					Log.d("URL", json.getUrl());
    					
    					return json.getJsonObject();
    				}
    				
    				protected void onPostExecute(JSONObject result) {
    					progress.dismiss();
    					int indeks = 0;
    					if (result != null)
    					{
    						try {
    							Daftar_sma sma = new Daftar_sma();
    							sma.setID_SEKOLAH(result.getInt("ID_SEKOLAH"));
    							
								Daftar_siswa siswa = new Daftar_siswa(result.getString("NO_TIKET"), 
										result.getString("NAMA_SISWA"), sma, result.getString("KELAS"), 
										result.getString("ALAMAT"), result.getInt("STATUS_KEDATANGAN"));
								
								status = siswa.getSTATUS_KEDATANGAN();
								
								if (status == 1)
								{
									showToast(siswa.getNAMA_SISWA()+getString(R.string.logged));
								}
								
								for (int i = 0; i < daftar_sma.size(); i++) {
									if (daftar_sma.get(i).getID_SEKOLAH() == siswa.getSEKOLAH().getID_SEKOLAH())
									{
										indeks = i;
										break;
									}
								}
								
								spinnerSekolah.setSelection(indeks);
								editNama.setText(siswa.getNAMA_SISWA());
								editKelas.setText(siswa.getKELAS());
								editAlamat.setText(siswa.getALAMAT());
								
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
    					}
    					else
    					{
    						showToast(getString(R.string.failed));
    					}
    					
    				};
    			}.execute(editNoTiket.getText().toString());
    		}
    		return false;
    	}
    }
    
    private class btnMasukClicked implements OnClickListener
    {
    	@Override
    	public void onClick(View arg0) {
    		// TODO Auto-generated method stub
    		
    		Daftar_sma sma = (Daftar_sma) spinnerSekolah.getSelectedItem();
    		
    		final Daftar_siswa siswa = new Daftar_siswa(editNoTiket.getText().toString(), 
    				editNama.getText().toString(), sma, editKelas.getText().toString(), editAlamat.getText().toString(), 
    				1);
    		
    		if (status == 0)
    		{
	    		
	    		
	    		AsyncTask<Daftar_siswa, String, JSONObject> task = new AsyncTask<Daftar_siswa, String, JSONObject>(){
	    			private ProgressDialog progress = new ProgressDialog(LoginActivity.this);
	    			
	    			@Override
	    			protected void onPreExecute() {
	    				// TODO Auto-generated method stub
	    				progress.setMessage(getString(R.string.loading));
	    				progress.setCancelable(false);
	    				progress.show();
	    			}
	    			
	    			@Override
	    			protected JSONObject doInBackground(Daftar_siswa... siswas) {
	    				// TODO Auto-generated method stub
	    				JSON json = aplikasi.getJson();
	    				json.setUrl(aplikasi.getDaftarUrl().get("updateSiswa"));
	    				
	    				HashMap<String, String> parameter = new HashMap<>();
	    				parameter.put("NO_TIKET", siswas[0].getNO_TIKET());
	                    parameter.put("NAMA_SISWA", siswas[0].getNAMA_SISWA());
	                    parameter.put("ID_SEKOLAH", ""+siswas[0].getSEKOLAH().getID_SEKOLAH());
	                    parameter.put("KELAS", siswas[0].getKELAS());
	                    parameter.put("ALAMAT", siswas[0].getALAMAT());
	                    parameter.put("STATUS_KEDATANGAN", ""+siswas[0].getSTATUS_KEDATANGAN());
	                    
	    				return json.getJsonObject("POST", parameter);
	    			}
	    			
	    			protected void onPostExecute(JSONObject result) {
	    				progress.dismiss();
	    				if (result != null)
	    				{
	    					try {
								if (result.getInt("STATUS") == 1)
								{
									showToast(getString(R.string.success));
									
									editAlamat.setText("");
									editNama.setText("");
									editKelas.setText("");
								}
								
								
								
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
	    				}
	    				else
	    				{
	    					showToast(getString(R.string.failed));
	    				}
	    			};
	    		}.execute(siswa);
    		}
    		else
    		{
    			showToast(siswa.getNAMA_SISWA()+getString(R.string.logged));
    		}
    	}
    }
  

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        aplikasi = new Aplikasi(LoginActivity.this);
        
        editNoTiket = (EditText) findViewById(R.id.editText7);
        editNoTiket.setOnKeyListener(new changeTyped());
        
        editNama = (EditText) findViewById(R.id.editText8);
        editKelas = (EditText) findViewById(R.id.editText9);
        editAlamat = (EditText) findViewById(R.id.editText10);
        spinnerSekolah = (Spinner) findViewById(R.id.spinne2);
        
        btnMasuk = (Button) findViewById(R.id.button4);
        btnMasuk.setOnClickListener(new btnMasukClicked());
        
        
        ActionBar actionBar = getSupportActionBar();
		 TextView tv = new TextView(getApplication());
		 
		 tv.setTextSize(20);
		 
		 
		 tv.setTextColor(Color.WHITE);
		 actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		 actionBar.setDisplayHomeAsUpEnabled(true);
		 
		 
		 tv.setText("Info Siswa");
		 actionBar.setCustomView(tv);
		 
		 AsyncTask<String, String, ArrayList<JSONObject>> task = new AsyncTask<String, String, ArrayList<JSONObject>>() {

	            private ProgressDialog progress = new ProgressDialog(LoginActivity.this);
	            @Override
	            protected void onPreExecute() {
	                progress.setMessage(getString(R.string.loading));
	                progress.setCancelable(false);
	                progress.show();
	            }

	            @Override
	            protected ArrayList<JSONObject> doInBackground(String... strings) {
	                JSON json = aplikasi.getJson();
	                json.setUrl(aplikasi.getDaftarUrl().get("getAllSMA"));
	                return (ArrayList<JSONObject>) json.getArrayJsonObject();
	            }

	            @Override
	            protected void onPostExecute(ArrayList<JSONObject> jsonObjectList) {

	                progress.dismiss();
	                
	                double max = 9999;
	                if (jsonObjectList.size() > 0)
	                {
	                    try
	                    {
	                        for (int i =0;i<jsonObjectList.size();i++)
	                        {
	                            Daftar_sma sma = new Daftar_sma();
	                            sma.setID_SEKOLAH(jsonObjectList.get(i).getInt("ID_SEKOLAH"));
	                            sma.setNAMA_SEKOLAH(jsonObjectList.get(i).getString("NAMA_SEKOLAH"));
	                            sma.setLATITUDE(jsonObjectList.get(i).getString("LATITUDE"));
	                            sma.setLONGITUDE(jsonObjectList.get(i).getString("LONGITUDE"));
	                            

	                            daftar_sma.add(sma);


	                        }


	                    }
	                    catch (Exception ex)
	                    {
	                        ex.printStackTrace();
	                    }

	                   

	                    spinnerSekolah.setAdapter(new SpinnerSMAAdapater(daftar_sma, LoginActivity.this));
	                    


	                }
	                else
	                {
	                    showToast(getString(R.string.failed));
	                }
	            }
	        }.execute("");
		 
		 
    }
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	pindahActivity(MainActivity.class);
    }
    
    @Override
    public void pindahActivity(Class class1) {
    	// TODO Auto-generated method stub
    	Intent intent = new Intent(LoginActivity.this, class1);
    	startActivity(intent);
    	finish();
    }
    
    @Override
    public void pindahActivity(Intent intent) {
    	// TODO Auto-generated method stub
    	startActivity(intent);
    	finish();
    }
    
    @Override
    public void showToast(String kata) {
    	// TODO Auto-generated method stub
    	Toast.makeText(LoginActivity.this, kata, Toast.LENGTH_SHORT).show();
    	
    }
}
