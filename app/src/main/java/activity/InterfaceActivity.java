package activity;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by ZANDUT on 12/2/2016.
 */

public interface InterfaceActivity {
    public void pindahActivity(Class class1);
    public void pindahActivity(Intent intent);
    public void showToast(String kata);
}
