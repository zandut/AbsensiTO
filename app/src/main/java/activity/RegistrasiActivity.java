package activity;

import JSON.JSON;
import adapter.SpinnerSMAAdapater;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import models.*;

import org.json.JSONObject;

import com.example.absensito.R;

import gps.GPSTracker;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ZANDUT on 12/2/2016.
 */

public class RegistrasiActivity extends AppCompatActivity implements InterfaceActivity{

    private EditText editNama;
    private EditText editKelas;
    private EditText editAlamat;
    private EditText editNoTiket;
    private Spinner spinnerSekolah;
    private Button btnSimpan;
    private ArrayList<Daftar_sma> daftar_sma = new ArrayList<>();
    private Aplikasi aplikasi;
    private GPSTracker gps;
    private double myLatitude, myLongitude;

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class btnSimpanOnClick implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {

            final Daftar_siswa siswa = new Daftar_siswa();
            siswa.setNO_TIKET(editNoTiket.getText().toString());
            siswa.setNAMA_SISWA(editNama.getText().toString());

            Daftar_sma sma = (Daftar_sma) spinnerSekolah.getSelectedItem();
            siswa.setSEKOLAH(sma);
            siswa.setKELAS(editKelas.getText().toString());
            siswa.setALAMAT(editAlamat.getText().toString());
            siswa.setSTATUS_KEDATANGAN(0);

            AsyncTask<Daftar_siswa, String, JSONObject> task = new AsyncTask<Daftar_siswa, String, JSONObject>() {

                private ProgressDialog progress = new ProgressDialog(RegistrasiActivity.this);

                @Override
                protected void onPreExecute() {
                    progress.setMessage("Loading ...");
                    progress.show();

                }

                @Override
                protected JSONObject doInBackground(Daftar_siswa... siswas) {
                    JSON json = aplikasi.getJson();
                    json.setUrl(aplikasi.getDaftarUrl().get("insertSiswa"));

                    HashMap<String, String> parameter = new HashMap<String, String>();
                    parameter.put("NO_TIKET", siswas[0].getNO_TIKET());
                    parameter.put("NAMA_SISWA", siswas[0].getNAMA_SISWA());
                    parameter.put("ID_SEKOLAH", ""+siswas[0].getSEKOLAH().getID_SEKOLAH());
                    parameter.put("KELAS", siswas[0].getKELAS());
                    parameter.put("ALAMAT", siswas[0].getALAMAT());
                    parameter.put("STATUS_KEDATANGAN", ""+siswas[0].getSTATUS_KEDATANGAN());



                    return json.getJsonObject("POST", parameter);
                }

                @Override
                protected void onPostExecute(JSONObject jsonObject) {
                    progress.dismiss();
                    if (jsonObject != null)
                    {
                        try {
                            if (jsonObject.getInt("STATUS") == 1)
                            {
                                showToast(getString(R.string.success));
                                editNoTiket.setText("");
                                editNama.setText("");
                                editAlamat.setText("");
                                editKelas.setText("");
                            }
                            else
                            {
                                showToast(getString(R.string.failed));
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                    else
                    {
                        showToast(getString(R.string.checkInternet));
                    }
                }
            }.execute(siswa);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        aplikasi = new Aplikasi(this);

        gps = new GPSTracker(this);





        editNama = (EditText) findViewById(R.id.editText);
        editNoTiket = (EditText) findViewById(R.id.editText3);
        editKelas = (EditText) findViewById(R.id.editText2);
        editAlamat = (EditText) findViewById(R.id.editText6);

        btnSimpan = (Button) findViewById(R.id.button3);
        btnSimpan.setOnClickListener(new btnSimpanOnClick());

        spinnerSekolah = (Spinner) findViewById(R.id.spinner);




        
		ActionBar actionBar = getSupportActionBar();
		 TextView tv = new TextView(getApplication());
		 
		 tv.setTextSize(20);
		 
		 
		 tv.setTextColor(Color.WHITE);
		 actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		 actionBar.setDisplayHomeAsUpEnabled(true);
		 
		 
		 tv.setText("Registrasi Siswa");
		 actionBar.setCustomView(tv);

        AsyncTask<String, String, ArrayList<JSONObject>> task = new AsyncTask<String, String, ArrayList<JSONObject>>() {

            private ProgressDialog progress = new ProgressDialog(RegistrasiActivity.this);
            @Override
            protected void onPreExecute() {
                progress.setMessage(getString(R.string.loading));
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            protected ArrayList<JSONObject> doInBackground(String... strings) {
                JSON json = aplikasi.getJson();
                json.setUrl(aplikasi.getDaftarUrl().get("getAllSMA"));
                return (ArrayList<JSONObject>) json.getArrayJsonObject();
            }

            @Override
            protected void onPostExecute(ArrayList<JSONObject> jsonObjectList) {

                progress.dismiss();
                int indeks = 0;
                double max = 9999;
                if (jsonObjectList.size() > 0)
                {
                    try
                    {
                        for (int i =0;i<jsonObjectList.size();i++)
                        {
                            Daftar_sma sma = new Daftar_sma();
                            sma.setID_SEKOLAH(jsonObjectList.get(i).getInt("ID_SEKOLAH"));
                            sma.setNAMA_SEKOLAH(jsonObjectList.get(i).getString("NAMA_SEKOLAH"));
                            sma.setLATITUDE(jsonObjectList.get(i).getString("LATITUDE"));
                            sma.setLONGITUDE(jsonObjectList.get(i).getString("LONGITUDE"));
                            sma.setDISTANCE(gps.getDistance(Double.parseDouble(sma.getLATITUDE()), Double.parseDouble(sma.getLONGITUDE())));

                            daftar_sma.add(sma);


                        }


                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    for (int i=0;i<daftar_sma.size();i++)
                    {
                        if (daftar_sma.get(i).getDISTANCE() < 0.3) {
                            if (max > daftar_sma.get(i).getDISTANCE()) {
                                max = daftar_sma.get(i).getDISTANCE();
                                indeks = i;
                            }
                        }
                    }

                    spinnerSekolah.setAdapter(new SpinnerSMAAdapater(daftar_sma, RegistrasiActivity.this));
                    spinnerSekolah.setSelection(indeks);


                }
                else
                {
                    showToast(getString(R.string.failed));
                }
            }
        }.execute("");

    }

    @Override
    public void onBackPressed() {
        pindahActivity(MainActivity.class);
    }

    @Override
    public void pindahActivity(Class class1) {
        Intent intent = new Intent(RegistrasiActivity.this, class1);
        startActivity(intent);
        finish();
    }

    @Override
    public void pindahActivity(Intent intent) {
        startActivity(intent);
        finish();
    }

    @Override
    public void showToast(String kata) {
        Toast.makeText(RegistrasiActivity.this, kata, Toast.LENGTH_SHORT).show();
    }
}
