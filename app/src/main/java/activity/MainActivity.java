package activity;

import java.util.ArrayList;
import java.util.List;

import android.Manifest.permission;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.absensito.R;

public class MainActivity extends AppCompatActivity implements
		InterfaceActivity {

	private Button btnRegistrasi;
	private Button btnMasukTO;

	private static final String[] DANGEROUS_PERMISSIONS = {

	permission.ACCESS_FINE_LOCATION

	};

	private void initPermissions() {
		List<String> missingPermissions = new ArrayList<String>();
		for (String permission : DANGEROUS_PERMISSIONS) {
			if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
				missingPermissions.add(permission);
			}
		}

		if (missingPermissions.size() > 0) {

			String[] permissions = new String[missingPermissions.size()];
			ActivityCompat.requestPermissions(this,
					missingPermissions.toArray(permissions), 1);
		} else {
			// we have all permissions, move on
		}
	}

	@TargetApi(23)
	@Override
	public void onRequestPermissionsResult(int requestCode,
			String[] permissions, int[] grantResults) {
		// TODO Auto-generated method stub
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

	}

	private class btnRegistrasiOnClick implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent(MainActivity.this,
					RegistrasiActivity.class);

			pindahActivity(intent);
		}
	}

	private class btnLoginOnClick implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent(MainActivity.this, LoginActivity.class);

			pindahActivity(intent);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initPermissions();

		btnRegistrasi = (Button) findViewById(R.id.button);
		btnRegistrasi.setOnClickListener(new btnRegistrasiOnClick());

		btnMasukTO = (Button) findViewById(R.id.button2);
		btnMasukTO.setOnClickListener(new btnLoginOnClick());

	}

	public Button getBtnMasukTO() {
		return btnMasukTO;
	}

	public Button getBtnRegistrasi() {
		return btnRegistrasi;
	}

	@Override
	public void showToast(String kata) {
		Toast.makeText(MainActivity.this, kata, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void pindahActivity(Class class1) {
		startActivity(new Intent(MainActivity.this, class1));
		finish();
	}

	@Override
	public void pindahActivity(Intent intent) {
		startActivity(intent);
		finish();
	}
}
