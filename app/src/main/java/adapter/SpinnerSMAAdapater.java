package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.absensito.R;

import models.Daftar_sma;



/**
 * Created by ZANDUT on 12/2/2016.
 */

public class SpinnerSMAAdapater extends BaseAdapter {
    private Context myContext;
    private ArrayList<Daftar_sma> array = new ArrayList<>();
    private LayoutInflater myInflate;

    public SpinnerSMAAdapater(ArrayList<Daftar_sma> array, Context myContext) {
        this.array = array;
        this.myContext = myContext;
        this.myInflate = LayoutInflater.from(this.myContext);

    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int i) {
        return array.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = myInflate.inflate(R.layout.row_adapter, viewGroup, false);
        TextView text = (TextView) v.findViewById(R.id.textView);
        text.setText(array.get(i).getNAMA_SEKOLAH());

        return v;
    }
}
