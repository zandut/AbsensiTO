package gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.text.DecimalFormat;

public class GPSTracker extends Service implements LocationListener {
	 
    private final Context mContext;
 
    // flag for GPS status
    boolean isGPSEnabled = false;
 
    // flag for network status
    boolean isNetworkEnabled = false;
 
    // flag for GPS status
    boolean canGetLocation = false;
 
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    private final double R = 6373;
    private DecimalFormat formatDecimal = new DecimalFormat("#.###");

 
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 5 meters
 
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 second
 
    // Declaring a Location Manager
    protected LocationManager locationManager;

    public double degToRad(double deg)
    {
        return deg * Math.PI / 180;
    }

    public double getDistance(double lat, double longi)
    {

        double radLat1 = degToRad(latitude), radLat2 = degToRad(lat), radLong1 = degToRad(longitude), radLong2 = degToRad(longi);
        double dLat = (radLat2 - radLat1);
        double dLong = (radLong2 - radLong1);

        double a = Math.pow(Math.sin(dLat/2), 2) + Math.cos(degToRad(latitude)) * Math.cos(degToRad(lat)) *
                Math.pow(Math.sin(dLong/2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = (c * R);

        return d;

    }
    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();
    }
    
//    public String getDistance(String origin, String destination) {
//
//	    String getdistance = "";
//	    String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin +"&destinations=" + destination + "&language=en-EN&units=metric&key=AIzaSyDS9m9fFP3AVU20ZIfv45rT4wAoK-06Ock";
//	    HttpGet httpGet = new HttpGet(url);
//	    HttpClient httpClient = new DefaultHttpClient();
//	    HttpResponse httpResponse;
//
//	    try {
//	        httpResponse = httpClient.execute(httpGet);
//	        HttpEntity httpEntity = httpResponse.getEntity();
//	        String line = EntityUtils.toString(httpEntity);
//
//	        JSONObject rootObject = new JSONObject(line);
//	        JSONArray rows = rootObject.getJSONArray("rows"); // Get all JSONArray rows
//
//	        for (int i = 0; i < rows.length(); i++) { // Loop over each each row
//	            Log.d("loop!", "rows");
//	            JSONObject row = rows.getJSONObject(i); // Get row object
//	            JSONArray elements = row.getJSONArray("elements"); // Get all elements for each row as an array
//	            for (int j = 0; j < elements.length(); j++) { // Iterate each element in the elements array
//	                Log.d("loop!", "elements");
//	                JSONObject element = elements.getJSONObject(j); // Get the element object
//	                JSONObject distance = element.getJSONObject("distance"); // Get distance sub object
//	                getdistance = String.valueOf(distance.getInt("value"));
//	            }
//
//	        }
//
//	    } catch (JSONException e) {
//	        e.printStackTrace();
//	    } catch (ClientProtocolException e) {
//	        e.printStackTrace();
//	    } catch (IOException e) {
//	        e.printStackTrace();
//	    }
//
//	    return getdistance;
//	}
 
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);
 
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
 
            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return location;
    }
     
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
        }       
    }
     
    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
         
        // return latitude
        return latitude;
    }
     
    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
         
        // return longitude
        return longitude;
    }
     
    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
        latitude = location.getLatitude();
        longitude = location.getLongitude();
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
     
    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */
   
 
   
}
