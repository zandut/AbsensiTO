package models;

/**
 * Created by ZANDUT on 12/2/2016.
 */

public class Daftar_sma {
    private int ID_SEKOLAH;
    private String NAMA_SEKOLAH;
    private String LATITUDE;
    private String LONGITUDE;
    private double DISTANCE;

    public Daftar_sma(int ID_SEKOLAH, String NAMA_SEKOLAH, String LATITUDE, String LONGITUDE, double DISTANCE) {
        this.ID_SEKOLAH = ID_SEKOLAH;
        this.NAMA_SEKOLAH = NAMA_SEKOLAH;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.DISTANCE = DISTANCE;
    }

    public Daftar_sma() {
    }

    public double getDISTANCE() {
        return DISTANCE;
    }

    public void setDISTANCE(double DISTANCE) {
        this.DISTANCE = DISTANCE;
    }

    public int getID_SEKOLAH() {
        return ID_SEKOLAH;
    }

    public void setID_SEKOLAH(int ID_SEKOLAH) {
        this.ID_SEKOLAH = ID_SEKOLAH;
    }

    public String getNAMA_SEKOLAH() {
        return NAMA_SEKOLAH;
    }

    public void setNAMA_SEKOLAH(String NAMA_SEKOLAH) {
        this.NAMA_SEKOLAH = NAMA_SEKOLAH;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    @Override
    public String toString() {
        return "Daftar_sma{" +
                "ID_SEKOLAH=" + ID_SEKOLAH +
                ", NAMA_SEKOLAH='" + NAMA_SEKOLAH + '\'' +
                ", LATITUDE='" + LATITUDE + '\'' +
                ", LONGITUDE='" + LONGITUDE + '\'' +
                '}';
    }
}
