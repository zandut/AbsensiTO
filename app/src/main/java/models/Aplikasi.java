package models;

import JSON.JSON;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

import com.example.absensito.R;



/**
 * Created by ZANDUT on 12/2/2016.
 */

public class Aplikasi {
    private JSON json = new JSON();
    private HashMap<String, String> daftarUrl = new HashMap<>();
    private Context context;
    private SharedPreferences sharedPreferences;
    private String url;

    public Aplikasi(Context context) {
        this.context = context;

        sharedPreferences = this.context.getSharedPreferences("session", Context.MODE_PRIVATE);
        url = "http://"+context.getString(R.string.host);

        daftarUrl.put("insertSiswa", url+"/insertsiswa");
        daftarUrl.put("getAllSMA", url+"/sma");
        daftarUrl.put("getAllSiswa", url+"/siswa");
        
        daftarUrl.put("updateSiswa", url+"/updatesiswa");

    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public Context getContext() {
        return context;
    }

    public HashMap<String, String> getDaftarUrl() {
        return daftarUrl;
    }

    public JSON getJson() {
    	json = new JSON();
        return json;
    }

    public void JsonReset()
    {
        json = new JSON();
    }
}
