package models;

/**
 * Created by ZANDUT on 12/2/2016.
 */

public class Daftar_siswa {
    private String NO_TIKET;
    private String NAMA_SISWA;
    private Daftar_sma SEKOLAH;
    private String KELAS;
    private String ALAMAT;
    private int STATUS_KEDATANGAN;

    public Daftar_siswa(String NO_TIKET, String NAMA_SISWA, Daftar_sma SEKOLAH, String KELAS, String ALAMAT, int STATUS_KEDATANGAN) {
        this.NO_TIKET = NO_TIKET;
        this.NAMA_SISWA = NAMA_SISWA;
        this.SEKOLAH = SEKOLAH;
        this.KELAS = KELAS;
        this.ALAMAT = ALAMAT;
        this.STATUS_KEDATANGAN = STATUS_KEDATANGAN;
    }

    public Daftar_siswa() {
    }

    public String getNO_TIKET() {
        return NO_TIKET;
    }

    public void setNO_TIKET(String NO_TIKET) {
        this.NO_TIKET = NO_TIKET;
    }

    public String getNAMA_SISWA() {
        return NAMA_SISWA;
    }

    public void setNAMA_SISWA(String NAMA_SISWA) {
        this.NAMA_SISWA = NAMA_SISWA;
    }

    public Daftar_sma getSEKOLAH() {
        return SEKOLAH;
    }

    public void setSEKOLAH(Daftar_sma SEKOLAH) {
        this.SEKOLAH = SEKOLAH;
    }

    public String getKELAS() {
        return KELAS;
    }

    public void setKELAS(String KELAS) {
        this.KELAS = KELAS;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public int getSTATUS_KEDATANGAN() {
        return STATUS_KEDATANGAN;
    }

    public void setSTATUS_KEDATANGAN(int STATUS_KEDATANGAN) {
        this.STATUS_KEDATANGAN = STATUS_KEDATANGAN;
    }



    @Override
    public String toString() {
        return "Daftar_siswa{" +
                "NO_TIKET='" + NO_TIKET + '\'' +
                ", NAMA_SISWA='" + NAMA_SISWA + '\'' +
                ", SEKOLAH=" + SEKOLAH +
                ", KELAS='" + KELAS + '\'' +
                ", ALAMAT='" + ALAMAT + '\'' +
                ", STATUS_KEDATANGAN=" + STATUS_KEDATANGAN +
                '}';
    }
}
